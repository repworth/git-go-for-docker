# Description
Docker image with: 

- git
- go

# Example Usage
```SHELL
docker run -it devopspec/git-go:git2.6-go1.6
```